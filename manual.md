# Map tiler web api service

This repo provides a web api service for tiling GeoImages (e.g. GeoTIFF) 
to map tiles which can be be used by web servers to display them to users

The solution is a containerized python webb app that uses command line tool to convert files.

The files are stored on mounted exchange folder. For demo purposes I use local directory, but in production, other file shaing tools can be used. E.g. Amazon S3 bucket or google storage bucket

# Usage

Create an empty directory and cd into it

Clone repo

```bash
git clone git clone git@bitbucket.org:maxsch3/maptiler.git
```

Create a mount directory, where you will be putting GeoImages and where tiled results will be stored
In real production environment it will be a shared location such as S3 or GCP storage bucket

```bash
mkdir mount
```

Build a docker image
```bash
docker build . -t my_tiler
```

Run a docker container
```bash
docker run -v "$(pwd)"/mount:/mnt -p 5000:5000 my_tiler
```

Copy some GeoTIFF into a ./mount folder which is shared with the running container.

To trigger the conversion you will need to send an http get request:

http://127.0.0.1:5000/tile_maps?source=cea.tif&target=layer2

Where source is the name of your source geoTiff file and target is the name of target directory where tiled map will be saved

cea.tif is available in git repo for demo purposes. It is just a random geotiff example I downloaded from the internet

Navigate to mount/layer2 folder and check results
