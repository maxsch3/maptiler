import subprocess
import os


class MapTiler:
    def __init__(self, def_loc="/mnt"):
        self.def_loc = def_loc
        pass

    def tile(self, source, target, **kwargs):
        """This function will call maptiler through OS command line"""
        result = subprocess.Popen(["maptiler", "-o", os.path.join(self.def_loc, target),
                                   os.path.join(self.def_loc, source)], stdout=subprocess.PIPE)
        # try:
        #     result = subprocess.run(["maptiler", "-o", self.def_loc + target, self.def_loc + source], capture_output=True)
        # except:
        #     raise OSError("Unexpected error: Could not run maptiler")

        return result.communicate()[0]

