from flask import Flask
from flask_restful import reqparse, Resource, Api
import tiler_interface as mt
from werkzeug.exceptions import BadRequest
import sys

app = Flask(__name__)
api = Api(app)


class MapTilerApi(Resource):

    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('source', required=True, type=str)
        parser.add_argument('target', required=True, type=str)
        args = parser.parse_args()
        try:
            out = mt.MapTiler().tile(source=args['source'], target=args['target'])
        except ValueError as e:
            raise BadRequest(e)
        return {'result': str(out)}


api.add_resource(MapTilerApi, '/tile_maps')


if __name__ == '__main__':
    debug = False
    host = '0.0.0.0'
    if 'debug' in sys.argv:
        host = None
        debug = True
    app.run(host=host, debug=debug)