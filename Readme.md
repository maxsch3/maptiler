# The challenge

The challenge has very specific technical gis component, map tiler, 
which I'm not familiar with yet. So far, I don't think getting deep into it is a
 good time spend as this alone will take me more than 2 hours. I will try to be more generic for now and focus on more generic architectural things.
 
 
## What is available?

The following components are available:
1. Web server, showing results to users. I assume the following:
        a. It is using output from MapTiler in folder format (quick inspection of provided html files pointed to MapTiler)
        b. It is hosted on private server and keeps all map tiles
2. Map tiles are stored on some cloud storage separately from the web server for better scalability and stability

## What needs to be done?

The following items have to be added:

1. Analytical packages that generate graphical data for analytical layers to be shown to users. Assimptions:
        a. Analytical packages are not tile-aware. They work on normal images, possibly of some standard size and resolution.
        b. Images have spatial location information: coordinates, rotation, scale
        c. Analytical packages are run in scheduled batch mode and not real-time after a request from user
2. A service for tiling outputs. It will convert raster images from analytics package into map tiles and save it as folder structure. 
3. Possibly a service for seamless upload of newly generated tiles to shared production storage. For now, I'm not sure if this service will be needed or not, but I will leave it here for reference.

### Now I need to decide what piece I will be focusing

I dont have enough inputs for any analytical package: no source images, no training data, no time, etc. So I will only design a standard interface for all analytical packages to use in the future. Seamless upload service would be very architecture specific and will depend on cloud provider, web server used, storage, etc. I'm not even sure if it is needed.

That leaves tiling service. This is a very good candidate to focus on: it is distributed as [Docker](https://hub.docker.com/r/maptiler/engine/) ready to be used, I have most of the inputs for designing this piece. 


## Map tiler service

For production solution, I will be creating a mictoservice - an isolated service loosely coupled through API. This approach has multiple benefits: 

1. Increased stability of the whole solution. Failure of this module will not affect main web server. The worst what could happen is web site would not receive updated maps from analytics packages
2. Using docker makes it easily scalable. Since this service will be used as backend batch-processing service, it can be taken down completely when not used. For example if all batches are running in daytime, or overnight, container instances can be taken down between those times to save on costs
3. Ease of upgrades as each part is independednt. This will also allow to stay agile even when your system matures and grows.

## MapTiler engine

MapTiler engine is a comman-line tool that does exactly what I want - it converts raw images into tiles and saves it into a folder structure, which can then be used by our web-site. 

According to their documentation, MapTiler accepts most of the image formats

There is no standalone installation available to download, so I can only use Docker container

Lets see what is inside this container

```bash
docker pull maptiler/engine
docker run -it -v "$(pwd)"/mount:/mnt maptiler/engine bash
```

For testing, I will be using one geotiff file downloaded from OS

```bash
# ls /mnt
cea.tif
```

Ok the folder is mounted and visible

lets try to convert that file

```bash
# maptiler -o /mnt/layer1 /mnt/cea.tif
There are 29 days remaining in your demo.

Opening          100 %   
Warping          100 %   

Rendering with 4 CPU threads. You can adjust this number with the -P option.

Rendering        100 %
```

Good. Looks like it is working!

This generated tiled maps and a bunch of html files that can be opened to navigate those maps
