FROM maptiler/engine

RUN apt update && apt install -y python3 python3-pip

WORKDIR /usr/src/app
COPY requirements.txt ./

EXPOSE 5000


RUN pip3 install --no-cache-dir -r requirements.txt

COPY tiler_interface.py api.py ./

CMD [ "python3", "./api.py" ]